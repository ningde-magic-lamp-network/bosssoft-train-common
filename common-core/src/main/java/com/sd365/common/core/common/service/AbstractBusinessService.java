/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: AbstractBusinessService
 * Author: Administrator
 * Date: 2023-02-10 08:55:51
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-02-10 08:55:51
 * <swagger> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.common.core.common.service;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.mybatis.PaginationAspect;
import com.sd365.common.core.common.advice.MyPageInfo;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.common.core.common.pojo.entity.BaseEntity;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import com.sd365.common.core.context.BaseContextHolder;

import java.util.Calendar;
import java.util.List;

/**
 * @ClassName: AbstractBusinessService
 * @Description: 将该类从各业务系统迁移到common为了完成对旧代码处理方式的兼容，
 * 建议优先使用注解来完成相应字段的赋值，该类的方法完成获得基础功能 例如 对通过字段进行赋值等
 * abel.zhan 2023-02-10从用户中心抽取了改功能，后续将重构各子系统避免在子系统定义该类
 * @Author: Administrator
 * @Date: 2023-02-10 08:55
 **/
public class AbstractBusinessService {
    public void baseDataStuff4Add(Object o) {
        //正常情况下系统的持久对象都是集成ＴenantBaseEntity即使是单租户系统也执行如此
        if (  o instanceof TenantBaseEntity) {
                TenantBaseEntity baseEntity = (TenantBaseEntity) o;
                baseEntity.setCreator(BaseContextHolder.getLoginUserName());
                baseEntity.setCreatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setCreatedTime(Calendar.getInstance().getTime());
                baseEntity.setModifier(BaseContextHolder.getLoginUserName());
                baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
                baseEntity.setModifier(BaseContextHolder.getLoginUserName());
                baseEntity.setTenantId(BaseContextHolder.getTanentId());
                baseEntity.setOrgId(BaseContextHolder.getOrgId());
                baseEntity.setCompanyId(BaseContextHolder.getCompanyId());
         } else if ( o instanceof BaseEntity) {// 程序可能存在一些特殊的情况集成BaseEntity
                BaseEntity baseEntity = (BaseEntity) o;
                baseEntity.setCreatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setCreatedTime(Calendar.getInstance().getTime());
                baseEntity.setCreator(BaseContextHolder.getLoginUserName());
                baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
                baseEntity.setModifier(BaseContextHolder.getLoginUserName());
        }

    }

    /**
     *  如果是修改者更新修改字段，至于版本号给mybatis插件自动维护，其他字段为业务类维护
     * @param o 持久对象通过通过service方法传入
     */
    public void baseDataStuff4Updated(Object o) {
        if (o instanceof BaseEntity) {
            BaseEntity baseEntity = (BaseEntity) o;
            baseEntity.setModifier(BaseContextHolder.getLoginUserName());
            baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
            baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
        }
    }


    public void baseDataStuffCommonPros(Object o) {

        if (o != null) {
            if (o instanceof TenantBaseEntity) {
                TenantBaseEntity baseEntity = (TenantBaseEntity) o;
                baseEntity.setTenantId(BaseContextHolder.getTanentId());
                baseEntity.setCompanyId(BaseContextHolder.getCompanyId());
                baseEntity.setOrgId(BaseContextHolder.getOrgId());
            } else if (o instanceof BaseEntity) {
                BaseEntity baseEntity = (BaseEntity) o;
                baseEntity.setCreatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setCreatedTime(Calendar.getInstance().getTime());
                baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
            }
        }
    }

    protected  CommonPage cast2CommonPage(Page page, List targetList){
        return new CommonPage(page.getTotal(),page.getPageNum(),page.getPageSize(),page.getPages(),targetList);
    }

    /**
     * 某一些方法需要先将Page的信息保存到BaseContextHolder
     * abel.zhan 2023-05-30 created
     * @param page mybatis 开始分页后查询返回的list
     */
    protected void setPageInfo2BaseContextHolder(Page page){
        BaseContextHolder.set(PaginationAspect.PAGE_INFO, JSON.toJSONString(new MyPageInfo(page.getTotal(),page.getPages())));
    }

}
