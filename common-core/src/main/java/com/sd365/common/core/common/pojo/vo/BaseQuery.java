/**
 * @file:  BaseQuery.java
 * @author: liang_xiaojian
 * @date:   2020/8/11 16:00
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.core.common.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.checkerframework.checker.units.qual.A;

/**
 * @class BaseQuery
 * @classdesc 查询参数的基类，该对象能否被查询基础框架判断并且应用分页
 * abel.zhan 增加 swagger注解
 * @author liang_xiaojian abel.zhan
 * @date 2020/8/11  16:00
 * @version 1.0.0
 * @see
 * @since
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseQuery extends AbstractQuery{
    /**
     *  当前页索引
     */
    @ApiModelProperty(value = "当前页码")
    private Integer pageNum = 1;
    /**
     * 每页的数据
     */
    @ApiModelProperty(value = "每页的记录数")
    private Integer pageSize = 10;
}
