/**
 * @file:  BaseDTO.java
 * @author: liang_xiaojian
 * @date:   2020/8/26 15:30
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.core.common.pojo.dto;

import com.alibaba.fastjson.JSON;
import com.sd365.common.core.common.constant.EntityConsts;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.checkerframework.checker.units.qual.A;

import java.io.Serializable;
import java.util.Date;

/**
 * @class BaseDTO
 * @classdesc xxDTO 从这里继承便于统一DTO接口和转型判断，XXDTO参数用于
 * @author liang_xiaojian
 * @date 2020/8/26  15:30
 * @version 1.0.0
 * @see
 * @since
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseDTO implements Serializable {

    private static final long serialVersionUID = -4183652945764463929L;

    @ApiModelProperty(value = "插入的这后端赋值 更新和删除则前端带回")
    private Long id;
    @ApiModelProperty(value = "状态 1 代表启用 0 代表禁用，这个")
    private Byte status= EntityConsts.INITIAL_STATUS;
    @ApiModelProperty(value="创建人id 后端赋值")
    private Long createdBy;
    private String creator;
    @ApiModelProperty(value = "创建时间后端赋值",example = "2018-10-0 12:18:48")
    private Date createdTime;
    @ApiModelProperty(value = "更新人后端赋值")
    private Long updatedBy;
    @ApiModelProperty(value = "更新人后名字后端赋值")
    private String modifier;
    @ApiModelProperty(value="更新日期后端赋值",example = "2018-10-01 12:18:48")
    private Date updatedTime;
    @ApiModelProperty(value="版本字段可能对应数据库字段 新增的后端初始为1",example = "2018-10-01 12:18:48")
    private Long version;

    @Override
    public String toString() {
       return JSON.toJSONString(this);
    }
}
