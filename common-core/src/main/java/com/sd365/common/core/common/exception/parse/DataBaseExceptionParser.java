package com.sd365.common.core.common.exception.parse;

import com.sd365.common.core.common.exception.DataBaseException;
import com.sd365.common.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.sql.SQLException;

/**
 * @class DataBaseExceptionParser
 * @classdesc 将统一异常捕捉的数据库异常转化为负责标准异常码定义的数据库异常
 * <p>
 *     算法流程：
 *    全局异常捕捉异常判断是否SQLException，如果是泽传递给parse分析出类从数据库表取得code 和 message，然后构建 DBerrorCode 从而构建
 *  DataBaseException  这个里面包含了 码和消息
 * </p>
 * @author Administrator
 * @date 2021-7-24  22:57
 * @version 1.0.0
 * @see
 * @since
 */
public class DataBaseExceptionParser implements ExceptionParser {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Override
    public DataBaseException parse(Exception sourceEx) {
        DataBaseException exception=null;
        String className=sourceEx.getStackTrace()[0].getClassName();
        // do your logic code
        if(sourceEx instanceof SQLException){
            SQLException sqlException=(SQLException) sourceEx;
            String moduleName="未定义";
            if(redisTemplate!=null && !StringUtil.isEmpty(redisTemplate.opsForValue().get(className))){
                moduleName=redisTemplate.opsForValue().get(className);
            }
            exception=new DataBaseException(String.valueOf(sqlException.getErrorCode()),
                    moduleName,
                    sqlException.getMessage(),sqlException.getCause());
        }else{
            exception=new DataBaseException("未定义数据库异常","未定义",sourceEx.getMessage(),sourceEx.getCause());
        }
        return exception;
    }
}
