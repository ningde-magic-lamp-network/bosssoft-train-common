/**
 * @file: AbstractController.java
 * @author: liang_xiaojian
 * @date: 2020/8/26 14:19
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.core.common.controller;

import com.github.pagehelper.Page;
import com.sd365.common.core.common.api.AppUtils;
import com.sd365.common.core.common.api.CommonResponse;
import com.github.pagehelper.page.PageMethod;
import com.sd365.common.core.context.BaseContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @class AbstractController
 * @classdesc 请分析所有的controller 可能存在哪些公用的功能
 * <br/> 定义通用的方法在该控制器下 例如分页插件的管理
 * <br/> 因为基础框架增加了 根据参数类型自动调用 PageHelper 所以 该类分页方法做了兼容处理
 * @author liang_xiaojian
 * @date 2020/8/26  14:19
 * @version 1.0.0
 * @see
 * @since
 */
public abstract class AbstractController {

    /**
     *  request 子类可以使用
     */
    private HttpServletRequest request;
    /**
     * 应答对象子类使用
     */
    private HttpServletResponse response;

    /**
     *  构造方法尝试获取请求和应答对象
     */
    public AbstractController(){
        RequestAttributes requestAttributes=RequestContextHolder.getRequestAttributes();
        if(!ObjectUtils.isEmpty(requestAttributes)){
            this.request=((ServletRequestAttributes)requestAttributes).getRequest();
            this.response=((ServletRequestAttributes) requestAttributes).getResponse();
        }
    }
    /**
     * 查询前判断是否已经被框架调用如果调用则不需要重复调用
     * @param pageIndex
     * @param pageSize
     */
    protected void doBeforePagination(int pageIndex, int pageSize) {
        if(!callBeforeMe()) {
            PageMethod.startPage(pageIndex, pageSize);
        }

    }

    protected Page doBeforePagination(int pageIndex, int pageSize,boolean count) {
        if(callBeforeMe()){
            return BaseContextHolder.get("page",Page.class);
        }else{
            return PageMethod.startPage(pageIndex, pageSize,count);
        }

    }

    private boolean callBeforeMe(){
        return BaseContextHolder.get("page")!=null;
    }

    protected CommonResponse<Object> buildCommonResponse(Object object) {
        CommonResponse<Object> commonResponse = new CommonResponse<>();
        // 调用工具类设置版本等信息
        AppUtils.setResponseExtendInfo(commonResponse);
        commonResponse.setBody(object);
        return commonResponse;
    }


}
