/**
 * @file:  DropdownVO.java
 * @author: liang_xiaojian
 * @date:   2020/9/24 23:36
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.core.common.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @class DropdownVO
 * @classdesc 由其作用得名，专为下拉框设计，常用于 id -> name
 * @author liang_xiaojian
 * @date 2020/9/24  23:36
 * @version 1.0.0
 * @see
 * @since
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DropdownVO  implements Serializable {
    private static final long serialVersionUID = -1095151363864965465L;

    /**
     * 数据项id
     */
    @ApiModelProperty(value = "下拉数据id项")
    private Long id;
    /**
     *  值
     */
    @ApiModelProperty(value = "下拉数据值")
    private String value;
}
