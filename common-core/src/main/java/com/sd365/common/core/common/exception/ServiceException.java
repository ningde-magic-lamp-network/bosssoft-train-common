package com.sd365.common.core.common.exception;

/**
 * @file: sd365-common-common-g05 com.sd365.common.core.common.exception
 * @author: Sukaiting
 * @date: 2020/8/26 16:03
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */

import com.sd365.common.core.common.exception.code.IErrorCode;
import com.sd365.common.core.common.exception.code.IModuleCode;

/**
 * @class BusinessException
 * @classdesc 基本业务操作异常，所有业务操作异常都继承于该类
 *  abel.zhan 2023-02-09 该类被BusinessExcepiton 替代
 * @author Sukaiting
 * @date 2020/8/26  16:03
 * @version 1.0.0
 * @see
 * @since
 */
@Deprecated
public class ServiceException extends AppException {
    private static final long serialVersionUID = 6304058622501786159L;

    public ServiceException() {
    }

    public ServiceException(String code, String message) {
        super(code, message);
    }

    public ServiceException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public ServiceException(IErrorCode errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public ServiceException(IModuleCode moduleCode, IErrorCode errorCode, Throwable cause) {
        super(moduleCode, errorCode, cause);
    }
}
