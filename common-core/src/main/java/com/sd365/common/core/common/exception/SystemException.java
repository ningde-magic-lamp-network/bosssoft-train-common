package com.sd365.common.core.common.exception;

/**
 * @file: sd365-common-common-g05 com.sd365.common.core.common.exception
 * @author: Sukaiting
 * @date: 2020/8/26 16:03
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */

import com.sd365.common.core.common.exception.code.IErrorCode;
import com.sd365.common.core.common.exception.code.IModuleCode;
/**
 * @class SystemException
 * @classdesc 偏向系统底层的错误异常
 * @author Sukaiting
 * @date 2020/8/26  16:03
 * @version 1.1.1
 * @see
 * @since
 */
public class SystemException extends AppException {
    private static final long serialVersionUID = 6304058622501786159L;

    /**
     *  空构造
     */
    public SystemException() {
    }

    /**
     * 系统异常构造方法
     * @param code 系统预定义的错误码
     * @param message 消息
     */
    public SystemException(String code, String message) {
        super(code, message);
    }

    /**
     * 统异常构造方法
     * @param code 系统预定义的错误码
     * @param message 消息
     * @param cause 错误具体信息
     */
    public SystemException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

    /**
     *  统异常构造方法
     * @param errorCode 错误枚举
     * @param cause 错误具体信息
     */
    public SystemException(IErrorCode errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    /**
     *  统一异常构造方法
     * @param moduleCode 模块编码
     * @param errorCode 错误码
     * @param cause 具体信息信息
     */
    public SystemException(IModuleCode moduleCode, IErrorCode errorCode, Throwable cause) {
        super(moduleCode, errorCode, cause);
    }
}
