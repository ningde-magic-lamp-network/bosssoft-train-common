package com.sd365.common.core.common.advice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Class MyPageInfo
 * @Description
 * @Author Administrator
 * @Date 2023-07-11  14:51
 * @version 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyPageInfo {
    /**
     *  总页数
     */
    private Long total;
    private int pages;
}
