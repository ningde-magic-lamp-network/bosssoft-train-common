/**
 * @file: BaseVO.java
 * @author: liang_xiaojian
 * @date: 2020/8/26 15:33
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.core.common.pojo.vo;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @class BaseVO
 * @classdesc 返回前端的vo类如果需要公共字段可以继承该字段
 *  abel.zhan 2023-02-09 增加了swagger注解
 * @author liang_xiaojian abel.zhan
 * @date 2020/8/26  15:33
 * @version 1.1.1
 * @see
 * @since
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseVO implements Serializable {

    private static final long serialVersionUID = -2361201137582920579L;
    /**
     *  id 一般雪花算法得到
     */
    @ApiModelProperty(value = "代表对象id对应表记录id字段")
    private Long id;
    /**
     *  创建人id
     */
    @ApiModelProperty(value = "代表对象id对应表记录id字段")
    private Long createdBy;
    /**
     * 创建人名字 引入他是避免都需要id关联查询
     */
    @ApiModelProperty(value = "代表创建者名字")
    private String creator;
    /**
     * 更新人id
     */
    @ApiModelProperty(value = "代表更新者id")
    private Long updatedBy;
    /**
     * 更新人名字
     */
    @ApiModelProperty(value = "代表修改者名字")
    private String modifier;
    /**
     * 记录生成的时间
     */
    @ApiModelProperty(value = "代表创建时间")
    private Date createdTime;
    /**
     * 记录修改的时间
     */
    @ApiModelProperty(value = "代表更新时间")
    private Date updatedTime;
    /**
     *  记录的状态 一般 0 代表停用 1 启用 还可以有其他的状态位
     */
    @ApiModelProperty(value = "状态")
    private Byte status;
    /**
     *  代表记录的版本号 由于控制并发操作记录
     */
    private Long version;
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
