package com.sd365.common.core.common.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @class TenantBaseVO
 * @classdesc  基于租户的系统的数据的VO从这里继承，包含了租户和组织
 * 机构以及公司最基本的信息，若非租户系统则从其他VO继承
 * @author Administrator
 * @date 2020-10-2  17:15
 * @version 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class TenantBaseVO extends BaseVO {
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private Long tenantId;
    /**
     *  机构id
     */

    @ApiModelProperty(value = "机构id")
    private Long orgId;
    /**
     *  公司id
     */
    @ApiModelProperty(value = "公司id")
    private Long companyId;
    /**
     *  租户名称
     */

    @ApiModelProperty(value = "租户id")
    private String tenantName;
    /**
     *  机构名
     */

    @ApiModelProperty(value = "机构id")
    private String OrgName;
    /**
     *  公司名
     */

    @ApiModelProperty(value = "公司id")
    private String companyName;
}
