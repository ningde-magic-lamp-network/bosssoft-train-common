package com.sd365.common.core.common.advice;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.mybatis.PaginationAspect;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.common.core.common.api.CommonResponse;
import com.sd365.common.core.common.api.CommonResponseUtils;
import com.sd365.common.core.context.BaseContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.List;
/**
 * @class DefaultUnifyResponseBodyAdvice
 * @classdesc 所以RestController 定义的统一应答报文通过该类生成,如果需要自定义应答则重写 beforeBodyWrite
 * @author Administrator
 * @date 2020-10-31  15:31
 * @version 1.0.0
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.sd365")
public class DefaultUnifyResponseBodyAdvice implements ResponseBodyAdvice<Object> {
    /**
     * need not wrapping
     */
    private  static final String NOWRAP="nowrap";
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    /**
     *   实现ResponseBodyAware 方法 对返回值进行统一的拦截
     *   <p>
     *     2021-12-07  增加了 If o instance of Page 代码 ，此代码解决之前版本需要service方法中
     *     多写这段代码的问题
     *     Page page = (Page) users;
     *     BaseContextHolder.set("pageInfo", JSON.toJSONString(new MyPageInfo(page.getTotal(), page.getPages())));
     *     后续基于1.1.0-RELEASE 将彻底解耦原来service 原来的service代码继续得到兼容
     *    <p/>
     * @param o 控制器方法返回的对象
     * @param methodParameter
     * @param mediaType
     * @param aClass
     * @param serverHttpRequest
     * @param serverHttpResponse
     * @return 系统包转的统一应答对象
     */

    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> aClass,
                                  ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        // 控制器进行过返回值处理所以不拦截
        if(o instanceof CommonResponse){
            return o;
        }else if(o instanceof CommonPage<?>){
            // 控制器直接返回了CommonPage对象 表示前端发起查询
            return CommonResponseUtils.success(o);
        }else{
            // 控制器直接返回普通对象 普通对象分为试图分页的对象和没有分页需求的对象
            try {
                /*
                  将Pagination 注解分离出来 后续代码可能就是 service方法加上该注解所以增加了 BaseContextHolder.get(PaginationAspect.PAGE_INFO)
                 判断条件
                 */
                return returnPaginationCommonResponse(o);
            }finally {
                // 这个判断是为了兼容以前的做法 新的@Pagination注解不再注入put page对象
                if(BaseContextHolder.get("page")!=null){
                    //清零分页请求对象
                    BaseContextHolder.set("page",null);
                }
                // 未来版本将只会使用这个判断,清零分页请求对象
                if(BaseContextHolder.get(PaginationAspect.PAGE_INFO)!=null){
                    BaseContextHolder.set(PaginationAspect.PAGE_INFO,null);
                }
            }
        }
    }

    /**
     *  处理controller即将返回的分页对象
     * @param o 控制器方法需要返回的对象
     * @return 包装成统一应答的对象
     */
    private Object returnPaginationCommonResponse(Object o) {
        if((BaseContextHolder.get("page")!=null ||
                BaseContextHolder.get(PaginationAspect.PAGE_INFO)!=null)
                && o instanceof List<?>){
            CommonPage<Object> commonPage=null;
            // 解决就在controller层返回分页对象的情况
            if(o instanceof Page){
               Page page=(Page)o;
               //2021-12-07 直接在构造传入避免方法内部需要依赖 BaseContext去取得分页的记录数
               commonPage=new CommonPage(page.getTotal(),page.getPageNum(),page);
           }else{
                // CommonPage构造会判别是否给list包装成为分页对象
               commonPage=new CommonPage((List)o);
           }
            return CommonResponseUtils.success(commonPage);
        }else{
            // fix by abel.zhan  2020-12-18  for authorization return boolean by feign call
            if(!StringUtils.isEmpty(BaseContextHolder.get(NOWRAP))){
                return o;
            }
            return CommonResponseUtils.success(o);
        }
    }

}
