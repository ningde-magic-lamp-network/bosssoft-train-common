package com.sd365.common.core.common.exception;

/**
 * @file: sd365-common-common-g05 com.sd365.common.core.common.exception
 * @author: abel.zhan
 * @date: 2020/8/26 16:03
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */

import com.sd365.common.core.common.exception.code.IErrorCode;
import com.sd365.common.core.common.exception.code.IModuleCode;
/**
 * @class DataBaseException
 * @classdesc
 * <p>
 *       依据设计文档增加数据库异常类，通过新增该类而非修改DaoException 避免修改引入异常
 *  </p>
 * @author abe.zhan
 * @date 2021-12-28  11:29
 * @version 1.0.0
 * @see
 * @since
 */
public class DataBaseException extends AppException {
    private static final long serialVersionUID = 6304058622501786159L;
    /**
     * 按设计文档约定行业和模块代码
     */
    private static final String MODULE_CODE="999999";
    public DataBaseException() {
    }

    public DataBaseException(String code, String message) {
        super(code, message);
    }

    public DataBaseException(String code, String moduleMessage,String message, Throwable cause) {
        // D 字符为设计文档约定属于数据库异常
        super(MODULE_CODE+"D"+code, moduleMessage+"-"+message, cause);
    }

    public DataBaseException(IErrorCode errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public DataBaseException(IModuleCode moduleCode, IErrorCode errorCode, Throwable cause) {
        super(moduleCode, errorCode, cause);
    }
}
