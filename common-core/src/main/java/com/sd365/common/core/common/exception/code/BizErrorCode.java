/**
 * @file: sd365-common-common-g05 com.sd365.common.core.common.exception.code
 * @author: abel.zhan
 * @date: 2021-7-24  21:58
 * @copyright: 2020-2033 www.ndsd365.com Inc. All rights reserved.
 */
package com.sd365.common.core.common.exception.code;


/**
 * @class BizErrorCode
 * @classdesc
 * @author Administrator
 * @date 2021-7-24  21:58
 * @version 1.0.0
 * @see
 * @since
 */
public enum BizErrorCode implements IErrorCode  {
    /**
     * 错误码、错误信息
     */
//    VERIFICATION_FAIL("B001","校验不通过"),
//    DEPEND_LACK("B002","依赖数据缺失"),
//    ACID_ERROR("B003","主键依赖数据缺失"),
//    BUSINESS_RULE_ERROR("B004","业务规则依赖缺失"),
    /**
     * 2023/02/13 wiki https://e.gitee.com/sd365_1/projects/305360/repos/sd365_1/sd365-common/wiki?sub_id=7749477
     * 新版本通用业务异常码设计
     */
    PARAM_VALID_FIELD_REQUIRE("B100","参数字段必须填写"),
    PARAM_VALID_FIELD_VIOLITE_RULE("B101","参数字段违反校验规则"),
    DATA_SEARCH_NOT_FOUND("B102","记录不存在"),
    DATA_SEARCH_FOUND_MANNY("B103","存在2条以上记录"),
    DATA_INSERT_FOUND_NO_UNIQUE_RECORD("B104","系统存在相同唯一标识的记录"),
    DATA_INSERT_RETURN_EFFECT_LATTER_ZERO("B105","插入记录返回影响数为0"),
    DATA_UPDATE_DUE_DUPLICATE_RECORD("B106","修改后记录和系统重复"),
    DATA_UPDATE_RETURB_EFFECT_LATTER_ZERO("B107","修改后记录和系统重复"),
    DATA_DELETE_NOT_FOUND("B108","系统没有找到指定的记录"),
    DATA_DELETE_FAIL_EXITS_REF_RECORD("B109","存在记录依赖不可删除"),
    DATA_DELETE_RETURB_EFFECT_LATTER_ZERO("B110","存在记录依赖不可删除"),
    /**
     * 在以上增加其它错误码
     */
    UNDEFINED("B000", "未定义");

    private String code;
    private String message;

    BizErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /** 依据错误码取得消息
     * @param: code 错误码
     * @return: 错误信息
     */
    public static String msg(String code) {
        for (BizErrorCode errorCode : BizErrorCode.values()) {
            if (errorCode.getCode().equals(code)) {
                return errorCode.message;
            }
        }
        return UNDEFINED.message;
    }
    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setCode(String code) {
        this.code=code;
    }

    @Override
    public void setMessage(String message) {
        this.message=message;
    }

}
