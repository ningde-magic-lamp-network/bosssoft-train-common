/**
 * @file:  ExceptionUtil.java
 * @author: liang_xiaojian
 * @date:   2020/9/9 17:13
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.core.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Objects;

/**
 * @class ExceptionUtil
 * @classdesc Common methods for exception.
 * <p> abel.zhan 2021-12-07 修改 PrinterStream的字符集为 UTF-8</p>
 * @author liang_xiaojian
 * @date 2020/9/9  17:13
 * @version 0.0.1
 */
@Slf4j
public class ExceptionUtil {

    private ExceptionUtil() {
        //
    }

    public static String getAllExceptionMsg(Throwable e) {
        Throwable cause = e;
        StringBuilder strBuilder = new StringBuilder();
        while (cause != null && !StringUtils.isEmpty(cause.getMessage())) {
            strBuilder.append("caused: ").append(cause.getMessage()).append(";");
            cause = cause.getCause();
        }

        return strBuilder.toString();
    }

    public static Throwable getCause(final Throwable t) {
        final Throwable cause = t.getCause();
        if (Objects.isNull(cause)) {
            return t;
        }
        return cause;
    }

    public static String getStackTrace(final Throwable t) {
        if (t == null) {
            return "";
        }
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final PrintStream ps;
        try {
            ps = new PrintStream(out, true, "UTF-8");
            t.printStackTrace(ps);
            return new String(out.toByteArray());
        } catch (UnsupportedEncodingException e) {
            log.info("ExceptionUtil",e);
            return "exception";
        }
    }
}
