package com.sd365.common.core.common.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @Class TenantBaseQuery
 * @Description 前端发送后端的查询参数继承该类，一般控制器的commonQuery方法接受该参数
 * @Author Administrator
 * @Date 2023-02-09  21:29
 * @version 1.1.1
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract  class  TenantBaseQuery extends BaseQuery {
    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id，前端一般不传该参数后端会从token解析")
    private Long tenantId;
    /**
     * 机构id
     */
    @ApiModelProperty(value = "租户id，前端一般不传该参数后端会从token解析")
    private Long orgId;
    /**
     * 公司id
     */
    @ApiModelProperty(value = "租户id，前端一般不传该参数后端会从token解析")
    private Long companyId;
}
