/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: ValidList
 * Author: Administrator
 * Date: 2023-02-28 20:55:29
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-02-28 20:55:29
 * <swagger> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.common.core.common.collection;

import javax.validation.Valid;
import java.util.*;

/**
 * @ClassName: ValidList
 * @Description: 使用该List替代需要被@Valid校验的List，用于controller接口和service接口
 * @Author: Administrator
 * @Date: 2023-02-28 20:55
 **/
public class ValidList<T> implements List {
    /**
     * 用于存储待校验参数的list
     */
    @Valid
    private List<T> sourceList =new ArrayList<>();


    public ValidList(@Valid List<T> validList) {
        this.sourceList = validList;
    }

    @Override
    public int size() {
        return sourceList.size();
    }

    @Override
    public boolean isEmpty() {
        return sourceList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return sourceList.contains(o);
    }

    @Override
    public Iterator iterator() {
        return sourceList.iterator();
    }

    @Override
    public T[] toArray() {
        return (T[]) sourceList.toArray();
    }

    @Override
    public boolean add(Object o) {
        return sourceList.add((T)o);
    }

    @Override
    public boolean remove(Object o) {
        return sourceList.remove(o);
    }

    @Override
    public boolean addAll(Collection c) {
        return sourceList.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return sourceList.addAll(index,c);
    }

    @Override
    public void clear() {
        sourceList.clear();
    }

    @Override
    public Object get(int index) {
        return sourceList.get(index);
    }

    @Override
    public T set(int index, Object element) {
        return sourceList.set(index,(T)element);
    }

    @Override
    public void add(int index, Object element) {
            sourceList.add(index,(T)element);
    }

    @Override
    public T remove(int index) {
        return sourceList.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return sourceList.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return sourceList.lastIndexOf(o);
    }

    @Override
    public ListIterator listIterator() {
        return sourceList.listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return sourceList.listIterator(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return sourceList.subList(fromIndex,toIndex);
    }

    @Override
    public boolean retainAll(Collection c) {
        return sourceList.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection c) {
        return sourceList.removeAll(c);
    }

    @Override
    public boolean containsAll(Collection c) {
        return sourceList.containsAll(c);
    }

    @Override
    public T[] toArray(Object[] a) {
        return (T[]) sourceList.toArray();
    }
}
