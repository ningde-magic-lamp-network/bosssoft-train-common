/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: Global
 * Author: Administrator
 * Date: 2023-05-17 20:30:56
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-05-17 20:30:56
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.common.constant;

/**
 * @ClassName: Global
 * @Description: 类的主要功能描述
 * @Author: Administrator
 * @Date: 2023-05-17 20:30
 **/
public class Global {
    /**
     *  角色id 作为标示hashmap的key
     */
    public static final String HASH_ROLE_ID_KEY="uc:cache:hash:role:id:";
    /**
     *  resource id作为缓存key
     */
    public static final String RESOURCE_ID_KEY="uc:cache:resource:id:";

    /**
     * 将通用的resource以hash方式存储
     */
    public static final String HASH_COMMON_RESOURCE_KEY="uc:cache:hash:common:resource:key:";
}
