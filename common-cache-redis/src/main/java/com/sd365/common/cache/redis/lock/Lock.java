/**
 * @file: Lock.java
 * @PackgeName: com.sd365.common.cache.redis.lock
 * @author: abel.zhan
 * @date: 2023/02/08
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.cache.redis.lock;

/**
 * @ClassName: Lock
 * @Author: abel.zhan
 * Date: 2020/8/27
 * project name: sd365-common-common-g05
 * @Version: 1.0.0
 * @Description: 锁接口
 */
public interface Lock {
    /**
     *  通过 set  key value nx exp time 获取锁 如果没获取这快速失败
     * @param key 如果null 或者 "" 这使用内部的key
     * @param expireTime 如果传入0 或者负数则采用内部的过期时间，具体过期时间根据业务长短掺入
     * @return 如果获取锁成功则返回true 否在false
     */
    boolean getLock(String key,  int expireTime);
    /**
     *  释放本次 调用获取的锁
     * @param key 如果没有指定则使用内部的key ，这个一定要等同于getLock使用的key
     * @return 如果删除成功则返回 true 否则 false 可能是过期 或者 删除失败
     */
    boolean releaseLock(String key);
}
