/**
 * @file: JedisDistributeLock.java
 * @author: linlu
 * @date: 2020/8/27
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.cache.redis.lock;

import com.sd365.common.cache.redis.util.JedisUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import java.util.Collections;

/**
 * @ClassName: JedisDistributeLock
 * @Author: ll
 * Date: 2020/8/27
 * project name: sd365-common-common-g05
 * @Version: 1.0.0
 * @Description: jedis实现分布式锁,因后续采用 spring集成 redisTemplate 所以考虑启用该jedis
 */
@Deprecated
public class JedisDistributeLock implements Lock{
    private static final String LOCK_SUCCESS = "OK";
    private static final String SET_IF_NOT_EXIST = "NX";
    private static final String SET_WITH_EXPIRE_TIME = "PX";
    private static final Long RELEASE_SUCCESS = 1L;
    private Jedis jedis = null;

    /**
     * 主机从主应用配置文件取得
     */
    @Value(value = "spring.redis.host")
    private String host;
    /**
     * 端口从主配置文件取得
     */
    @Value(value = "spring.redis.port")
    private int port;
    /**
     *
     */
    @Value(value = "spring.redis.timeout")
    private int timeout;

    @Override
    public boolean getLock(String key, int expireTime) {
        return false;
    }

    @Override
    public boolean releaseLock(String key) {
        return false;
    }
}
