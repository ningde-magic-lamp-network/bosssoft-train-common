/**
 * @file: JedisDistributeLock.java
 * @PackgeName: com.sd365.common.cache.redis.lock
 * @author: linlu
 * @date: 2020/8/27
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.cache.redis.lock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

/**
 * @ClassName: RedisDistributeLock
 * @Author: abel.zhan
 * Date: 2020/8/27
 * project name: sd365-common-common-g05
 * @Version: 1.0.0
 * @Description: RedisTemplate 实现快速失败分布式锁
 */
@Component
public class RedisDistributeLock implements Lock{
    /**
     * 每次调用getLock产生 identityValue
     */
    private String identityValue;
    /**
     * 锁名称
     */
    public static final String REDIS_KEY = "common:cache:redis:lock";
    /**
     * 加锁失效时间，毫秒
     */
    public static final int LOCK_EXPIRE = 300;
    /**
     * 注入缓存操作类，具体的缓存配置依赖外部的主配置文件
     */
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Override
    public  boolean getLock(String key,int expireTime) {
        String redisKey = StringUtils.isEmpty(key) ? REDIS_KEY : key;
        identityValue=UUID.randomUUID().toString();
        return redisTemplate.opsForValue().setIfAbsent(redisKey,identityValue , Duration.ofSeconds(expireTime <= 0 ? LOCK_EXPIRE : expireTime));
    }
    @Override
    public boolean releaseLock(String key) {
        String redisKey=StringUtils.isEmpty(key) ? REDIS_KEY : key;
        String luaScript = String.format("if redis.call('get', %s) == %s then return redis.call('del', %s) else return 0 end",redisKey,identityValue,identityValue);
        RedisScript<Long> redisScript = new DefaultRedisScript<>(luaScript,Long.class);
        return   this.redisTemplate.execute(redisScript, Collections.singletonList(key),identityValue)>0;
    }

}
