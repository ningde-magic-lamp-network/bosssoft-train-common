/**
 * @file: JedisUtil.java
 * @author: linlu
 * @date: 2020/8/27
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
package com.sd365.common.cache.redis.util;

import org.springframework.beans.factory.annotation.Value;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
/**
 * @class JedisUtil
 * @classdesc <p> 2021-12-07 去除 port变量</p><br><p> 2023-02-8日废弃Jedis分布式锁所以也废除JedisUtil工具类</p>
 * @author Administrator
 * @date 2021-12-7  22:00
 * @version 1.0.0
 * @see
 * @since
 */
@Deprecated
public class JedisUtil {
   // private int port;
    private static JedisPool jedisPool ;
    private static JedisPoolConfig jedisPoolConfig;
    static {
        jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(10000);
        jedisPoolConfig.setMaxWaitMillis(10000);
        jedisPoolConfig.setMaxIdle(1000);
    }

    public static Jedis getResource(String host,int port,int timeout ){
           jedisPool = new JedisPool(jedisPoolConfig,host,port,timeout);
           return  jedisPool.getResource();
    }

}
