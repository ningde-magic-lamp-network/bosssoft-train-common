/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: EncryptSimpleUtil
 * Author: Administrator
 * Date: 2023-02-28 22:01:24
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-02-28 22:01:24
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.common.util;

import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.codec.digest.Sha2Crypt;

/**
 * @ClassName: EncryptSimpleUtil
 * @Description: 包含常用的加密算法 MD5 SHA ,DES ,AES ,BASE64,HEX 等
 * @Author: Administrator
 * @Date: 2023-02-28 22:01
 **/
public abstract class EncryptSimpleUtil {
    /**
     *  md5
     * @param key  加密内容
     * @param salt  盐
     * @return 加密成功的内容
     */
    public static String md5(byte[] key,String salt){
        return Md5Crypt.apr1Crypt(key,salt);
    }

    /**
     *  sha2 加密 调用 apache common完成
     * @param key 加密内容
     * @param salt 盐
     * @return
     */
    public static String sha2(byte[] key,String salt){
        return Sha2Crypt.sha256Crypt(key,salt);
    }

}
