/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: AbstractTokenBuilder
 * Author: Administrator
 * Date: 2023-03-07 15:22:01
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-03-07 15:22:01
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.common.util;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @ClassName: AbstractTokenBuilder
 * @Description: 类的主要功能描述
 * @Author: Administrator
 * @Date: 2023-03-07 15:22
 **/
public abstract class AbstractTokenBuilder {
    /**
     * 设置 token头部信息
     * @param head  头部信息，描述类型和算法
     * @return 对象引用 支持builder模式连续调用其他方法
     */
    public abstract AbstractTokenBuilder buildHead(JSONObject head);

    /**
     *  构建负载
     * @param payload  要求参数以 fastjson的 JasonObject 模式
     * @return 对象引用 支持builder模式连续调用其他方法
     */
    public abstract AbstractTokenBuilder buildPayload(JSONObject payload);

    /**
     *  token签名实现
     * @param secret 用于签名的秘钥
     * @return  签名完成后的对象引用
     */
    public abstract AbstractTokenBuilder buildSignature(String secret);

    /**
     * 返回 this 引用 这个最后一步调用
     * @return this引用
     */
    public abstract AbstractTokenBuilder build();

    /**
     *   基于传入的参数head payload 验证 签名是否一致
     * @param head  JWT token协议头
     * @param payload  JWT token 有效载荷
     * @param signature 待验证的token的签名
     * @param secret 用于验证的秘钥
     * @return  true签名一直 false签名不一直
     */
    public abstract boolean verifySignature(JSONObject head,JSONObject payload,String signature,String secret);

    /**
     *  基于token本身验证签名，基于该对象自身的head和payload 集合secret 进行签名验证
     * @param signature 当前待验证的签名
     * @param secret  用于验证签名是否一致
     * @return true 签名一致 false 签名不一致 可能是内容被篡改
     */
    public abstract boolean verifySignature(String signature,String secret);

    /**
     *  输出 JSON对象格式
     * @return head，payload ，signature组成json对象输出
     */
    public abstract JSONObject toJSONObject();

    /**
     *  将 token的 head ,payload ,signature 装入 list返回
     * @return 包含 JwtToken写协议3块内容
     */
    public abstract List<String> toJSONList();
    /**
     *  产生签名用于比对签名是否一致
     * @param head  用于产生签名的token的 head  各位 JSONString
     * @param payload 用于产生签名的token的 payload  各位 JSONString
     * @param secret 用于产生签名的 秘钥
     * @return 生成的签名字符串
     */
    protected abstract String generateSignature(String head , String payload, String secret);
}
