/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: AliyunSmsUtil
 * Author: Administrator
 * Date: 2023-03-01 16:34:31
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-03-01 16:34:31
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.common.util;

import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName: AliyunSmsUtil
 * @Description: 封装了阿里云短信功能
 * @Author: Administrator
 * @Date: 2023-03-01 16:34
 **/
@Slf4j
public class AliyunSmsUtil {
        // 产品名称:云通信短信API产品,开发者无需替换
        static final String product = "Dysmsapi";
        // 产品域名,开发者无需替换
        static final String domain = "dysmsapi.aliyuncs.com";

        // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
        private static final String accessKeyId = "<aliyunsms.AccessKeyId>";//需要替换
        private static final String accessKeySecret = "<aliyunsms.AccessKeySecret>";//需要替换
        // 签名
        private static final String signName = "<aliyunsms.signName>";//需要替换

        /**
         * 发送短信验证码
         * @param phoneNum
         * @param verifyCode
         * @param templateCode
         * @return
         * @throws ClientException
         */
        public static SendSmsResponse sendVerifyCode(String phoneNum, String verifyCode, String templateCode)
                throws ClientException {
            // 可自助调整超时时间
            System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
            System.setProperty("sun.net.client.defaultReadTimeout", "10000");

            // 初始化acsClient,暂不支持region化
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);

            // 组装请求对象-具体描述见控制台-文档部分内容
            SendSmsRequest request = new SendSmsRequest();
            // 必填:待发送手机号
            request.setPhoneNumbers(phoneNum);
            // 必填:短信签名-可在短信控制台中找到
            request.setSignName(signName);
            // 必填:短信模板-可在短信控制台中找到
            request.setTemplateCode(templateCode);
            // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            request.setTemplateParam("{code:" + verifyCode + "}");

            // 选填-上行短信扩展码(无特殊需求用户请忽略此字段)
            // request.setSmsUpExtendCode("90997");

            // 可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
            //request.setOutId("yourOutId");

            // hint 此处可能会抛出异常，注意catch
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            return sendSmsResponse;
        }

        /**
         * 查询消息的发送状态
         * @param phoneNum
         * @param bizId 发送回执id,调用发送短信api时,会返回
         * @return
         * @throws ClientException
         */
        public static QuerySendDetailsResponse querySendDetails(String phoneNum, String bizId) throws ClientException {

            // 可自助调整超时时间
            System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
            System.setProperty("sun.net.client.defaultReadTimeout", "10000");

            // 初始化acsClient,暂不支持region化
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);

            // 组装请求对象
            QuerySendDetailsRequest request = new QuerySendDetailsRequest();
            // 必填-号码
            request.setPhoneNumber(phoneNum);
            // 可选-流水号
            request.setBizId(bizId);
            // 必填-发送日期 支持30天内记录查询，格式yyyyMMdd
            SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
            request.setSendDate(ft.format(new Date()));
            // 必填-页大小
            request.setPageSize(10L);
            // 必填-当前页码从1开始计数
            request.setCurrentPage(1L);

            // hint 此处可能会抛出异常，注意catch
            QuerySendDetailsResponse querySendDetailsResponse = acsClient.getAcsResponse(request);

            return querySendDetailsResponse;
        }

        public static void main(String[] args) throws ClientException, InterruptedException {

            // 发验证码短信
            SendSmsResponse response = sendVerifyCode("15900000000", "123456","SMS_123456789");
            log.info("短信接口返回的数据----------------");
            log.info("Code=" + response.getCode());
            log.info("Message=" + response.getMessage());
            log.info("RequestId=" + response.getRequestId());
            log.info("BizId=" + response.getBizId());//消息回执id

            Thread.sleep(3000L);

            // 查明细
            if (response.getCode() != null && response.getCode().equals("OK")) {
                QuerySendDetailsResponse querySendDetailsResponse = querySendDetails("15900000000", response.getBizId());
                log.info("短信明细查询接口返回数据----------------");
                log.info("Code=" + querySendDetailsResponse.getCode());
                log.info("Message=" + querySendDetailsResponse.getMessage());
                int i = 0;
                for (QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse
                        .getSmsSendDetailDTOs()) {
                    log.info("SmsSendDetailDTO[" + i + "]:");
                    log.info("Content=" + smsSendDetailDTO.getContent());
                    log.info("ErrCode=" + smsSendDetailDTO.getErrCode());
                    log.info("OutId=" + smsSendDetailDTO.getOutId());
                    log.info("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
                    log.info("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
                    log.info("SendDate=" + smsSendDetailDTO.getSendDate());
                    log.info("SendStatus=" + smsSendDetailDTO.getSendStatus());
                    log.info("Template=" + smsSendDetailDTO.getTemplateCode());
                }
                log.info("TotalCount=" + querySendDetailsResponse.getTotalCount());
                log.info("RequestId=" + querySendDetailsResponse.getRequestId());
            }
        }

}
