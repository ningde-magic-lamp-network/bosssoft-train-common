package com.sd365.common.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;

import static org.junit.jupiter.api.Assertions.*;
class JwtTokenBuilderTest {

    private static AbstractTokenBuilder tokenBuilder=new JwtTokenBuilder();

    static final String JWT_TOKEN="{\"typ\":\"JWT\",\"alg\":\"sh2\"}.{\"name\":\"jim\",\"id\":1000}.$5$0$2j75w1ijK8JjJJR2Bni4kGGgzMRY.LsJJ0x8lmpJP13";
    static final String SALT="$6$0$";

    @BeforeAll
    static void init() throws UnsupportedEncodingException {
        JSONObject head=new JSONObject();
        head.put("alg","sh2");
        head.put("typ","JWT");
        JSONObject payLoad=new JSONObject();
        payLoad.put("id",1000);
        payLoad.put("name","jim");

        tokenBuilder.buildHead(head).buildPayload(payLoad).buildSignature("$6$0$");

    }
    @Test
    void build() throws UnsupportedEncodingException {
        JSONObject head=new JSONObject();
        head.put("alg","sh2");
        head.put("typ","JWT");
        JSONObject payLoad=new JSONObject();
        payLoad.put("id",1000);
        payLoad.put("name","jim");
        String token=tokenBuilder.build().toString();
        Assert.assertEquals(JWT_TOKEN,token);

    }

    @Test
    void verifySignature() {

        Assert.assertTrue(
                tokenBuilder.verifySignature("$5$0$2j75w1ijK8JjJJR2Bni4kGGgzMRY.LsJJ0x8lmpJP13",SALT));
    }

    @Test
    void verifySignature1() {
        JSONObject head=new JSONObject();
        head.put("alg","sh2");
        head.put("typ","JWT");
        JSONObject payLoad=new JSONObject();
        payLoad.put("id",1000);
        payLoad.put("name","jim");
        Assert.assertTrue(tokenBuilder.verifySignature(head,payLoad,"$5$0$2j75w1ijK8JjJJR2Bni4kGGgzMRY.LsJJ0x8lmpJP13",SALT));

    }

    @Test
    void toJSONObject() {
      Assert.assertTrue(tokenBuilder.toJSONObject().get("payload").equals("{\"name\":\"jim\",\"id\":1000}"));
    }

    @Test
    void toJSONList() {
       Assert.assertTrue(tokenBuilder.toJSONList().get(1).equals("{\"name\":\"jim\",\"id\":1000}"));

    }


    @Test
    void generateSignature() {
        JSONObject head=new JSONObject();
        head.put("alg","sh2");
        head.put("typ","JWT");
        JSONObject payLoad=new JSONObject();
        payLoad.put("id",1000);
        payLoad.put("name","jim");
       Assert.assertTrue("$5$0$2j75w1ijK8JjJJR2Bni4kGGgzMRY.LsJJ0x8lmpJP13"
               .equals(tokenBuilder.generateSignature(head.toJSONString(),payLoad.toJSONString(),SALT)));
    }
}