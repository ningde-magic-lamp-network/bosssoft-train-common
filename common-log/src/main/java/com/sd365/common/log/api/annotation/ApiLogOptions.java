/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: ApiLogOptions
 * Author: Administrator
 * Date: 2023-07-19 21:25:31
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-07-19 21:25:31
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.common.log.api.annotation;

/**
 * @EnumName: ApiLogOptions
 * @Description: 用于改进ApiLog注解的灵活性，可以通过注解关闭相关功能例如分页
 * @Author: Administrator
 * @Date: 2023-07-19 21:25
 **/
public enum ApiLogOptions {
    /**
     * 仅仅用于日志 默认设置
     */
    ONLY_LOG,
    /**
     * 开启后包含调用PageMethod.startPage
     */
    INCLUDE_PAGINATION
}
