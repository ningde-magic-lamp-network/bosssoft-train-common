package com.sd365.common.log.api.annotation;
/**
 * @file:  ApiLog.java
 * @author: chh
 * @date:   2020/8/26 14:43
 * @copyright: 2020-2023 www.bosssoft.com.cn Inc. All rights reserved.
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @class ApiLog
 * @classdesc 用于打印请求和响应日志的注解
 * abel.zhan 2023-07-25 增加 option用于配置是否启用分页
 * 举例：
 * “@ApiLog + 某方法” 默认只开启log
 * “@ApiLog (value=ApiLogOptions.INCLUDE_PAGINATION) + 某方法” 支持内部调用分页，此举是为兼容旧系统代码
 * @author chh
 * @date 2020/8/26  14:43
 * @version 1.0.0
 * @see
 * @since
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiLog {
    /**
     *  增加选项开关灵活控制ApiLog对旧代码的兼容性
     * @return 选择项 默认支持log模式
     */
    ApiLogOptions option() default ApiLogOptions.ONLY_LOG;
}
